from unittest import TestCase
from unittest.mock import patch, Mock
from flask import Flask
from flask_feature_flag.use_cases import (
    GetFeature,
    CreateFeatureMongo,
    UpdateFeatureMongo,
    ValidateFeatureMongo,
    DeleteFeatureMongo
)


class TestGetFeature(TestCase):

    def test_flask_config_enable(self):
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAGS'] = {'ENV_HI': True}
        self._app.config['FEATURE_FLAG_TYPE'] = 'FLASK_CONFIG'

        with self._app.app_context():
            use_cases = GetFeature(self._app.config, Mock)
            result = use_cases.handle('ENV_HI')

            self.assertEqual(result, True)

    def test_flask_config_desable(self):
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAGS'] = {'ENV_HI': False}
        self._app.config['FEATURE_FLAG_TYPE'] = 'FLASK_CONFIG'

        with self._app.app_context():
            use_cases = GetFeature(self._app.config, Mock)
            result = use_cases.handle('ENV_HI')

            self.assertEqual(result, False)

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_flask_mongo_cache(self, mock_obj):
        # mocks
        mock_cache = Mock()
        mock_cache.get.return_value = True

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_cases = GetFeature(self._app.config, mock_cache)
            result = use_cases.handle('ENV_HI')

            self.assertEqual(result, True)
            mock_cache.get.assert_called_with('ENV_HI')

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_flask_mongo_no_cache(self, mock_obj):
        # mocks
        mock_cache = Mock()
        mock_cache.get.return_value = None
        mock_obj.return_value = Mock(
            feature_flag_value=True
        )

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_cases = GetFeature(self._app.config, mock_cache)
            result = use_cases.handle('ENV_HI')

            self.assertEqual(result, True)
            mock_obj.assert_called_with(feature_flag='ENV_HI')
            mock_cache.get.assert_called_with('ENV_HI')

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_flask_not_mongo(self, mock_obj):
        # mocks
        mock_cache = Mock()
        mock_cache.get.return_value = None
        mock_cache.set.return_value = None
        mock_obj.return_value = None

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_cases = GetFeature(self._app.config, mock_cache)
            result = use_cases.handle('ENV_HI')

            self.assertEqual(result, True)
            mock_obj.assert_called_with(feature_flag='ENV_HI')
            mock_cache.get.assert_called_with('ENV_HI')


class TestCreateFeatureMongo(TestCase):

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    @patch('flask_feature_flag.use_cases.FeatureFlag')
    def test_ok(self, mock_obj, mock_repo):
        # mocks
        mock_repo.return_value = None
        mock_cache = Mock()
        mock_cache.set.return_value = None

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        request = {
            "feature_flag": "ENV_HI",
            "feature_flag_value": True
        }

        with self._app.app_context():
            use_cases = CreateFeatureMongo(mock_cache)
            result = use_cases.handle(request)

            self.assertEqual(result.http_code, 201)
            mock_repo.assert_called_with(feature_flag='ENV_HI')
            mock_cache.set.assert_called_with(
                'ENV_HI', True, timeout=0
                )
            mock_obj.return_value.save.assert_called_with()

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    @patch('flask_feature_flag.use_cases.FeatureFlag')
    def test_forbidden(self, mock_obj, mock_repo):
        # Mocks
        mock_repo.return_value = True
        mock_cache = Mock()
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        request = {
            "feature_flag": "ENV_HI",
            "feature_flag_value": True
        }

        with self._app.app_context():
            use_cases = CreateFeatureMongo(mock_cache)
            result = use_cases.handle(request)

            self.assertEqual(result.http_code, 403)
            mock_repo.assert_called_with(feature_flag='ENV_HI')

    @patch('flask_feature_flag.use_cases.FeatureFlag')
    def test_bad_request(self, mock_obj):
        # Mocks
        mock_cache = Mock()
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        request = {}

        with self._app.app_context():
            use_cases = CreateFeatureMongo(mock_cache)
            result = use_cases.handle(request)

            self.assertEqual(result.http_code, 400)
            self.assertIsNotNone(result.errors)


class TestUpdateFeatureMongo(TestCase):

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_ok(self, mock_obj):
        # mocks
        mock_cache = Mock()
        mock_cache.set.return_value = None
        mock_result = Mock(
            feature_flag_value=True
            )
        mock_result.save.return_value = None
        mock_obj.return_value = mock_result

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        request = {
            "feature_flag": "ENV_HI",
            "feature_flag_value": True
        }

        with self._app.app_context():
            use_cases = UpdateFeatureMongo(mock_cache)
            result = use_cases.handle(request)

            self.assertEqual(result.http_code, 200)
            mock_cache.set.assert_called_with(
                'ENV_HI', True, timeout=0)
            mock_result.save.assert_called_with()
            mock_obj.assert_called_with(feature_flag='ENV_HI')

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_not_found(self, mock_obj):
        # mocks
        mock_obj.return_value = None
        mock_cache = Mock()

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        request = {
            "feature_flag": "ENV_HI",
            "feature_flag_value": True
        }

        with self._app.app_context():
            use_cases = UpdateFeatureMongo(mock_cache)
            result = use_cases.handle(request)

            self.assertEqual(result.http_code, 404)
            mock_obj.assert_called_with(feature_flag='ENV_HI')

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_bad_request(self, mock_obj):
        # Mocks
        mock_cache = Mock()
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        request = {}

        with self._app.app_context():
            use_cases = UpdateFeatureMongo(mock_cache)
            result = use_cases.handle(request)

            self.assertEqual(result.http_code, 400)
            self.assertIsNotNone(result.errors)


class TestValidateFeatureMongo(TestCase):

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_ok(self, mock_repo):
        # mocks
        mock_cache = Mock()
        mock_cache.get.return_value = True
        mock_repo.return_value = Mock(
            feature_flag_value=True
            )

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_case = ValidateFeatureMongo(mock_cache)
            result = use_case.handle({'feature_flag': 'ENV_HI'})
            data = {
                'in_cache': True,
                'in_cache_value': True,
                'in_mongo': True,
                'in_mongo_value': True
                }

            self.assertEqual(result.http_code, 200)
            self.assertEqual(result.message, 'ok')
            self.assertEqual(result.data, data)
            mock_cache.get.assert_called_with(
                'ENV_HI'
            )
            mock_repo.assert_called_with(
                feature_flag='ENV_HI'
            )

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_ok_false(self, mock_repo):
        # mocks
        mock_cache = Mock()
        mock_cache.get.return_value = None
        mock_repo.return_value = None

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_case = ValidateFeatureMongo(mock_cache)
            result = use_case.handle({'feature_flag': 'ENV_HI'})
            data = {
                'in_cache': None,
                'in_cache_value': None,
                'in_mongo': None,
                'in_mongo_value': None
                }

            self.assertEqual(result.http_code, 200)
            self.assertEqual(result.message, 'ok')
            self.assertEqual(result.data, data)
            mock_cache.get.assert_called_with(
                'ENV_HI'
            )
            mock_repo.assert_called_with(
                feature_flag='ENV_HI'
            )

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_bad_request(self, mock_repo):
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_case = ValidateFeatureMongo(Mock())
            result = use_case.handle({})

            self.assertEqual(result.http_code, 400)
            self.assertEqual(result.message, 'bad_request')


class TestDeleteFeatureMongo(TestCase):

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_ok(self, mock_repo):
        # mocks
        mock_cache = Mock()
        mock_cache.delete.return_value = None
        mock_result_repo = Mock(
            feature_flag_value=True
            )
        mock_result_repo.delete.return_value = None
        mock_repo.return_value = mock_result_repo

        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_case = DeleteFeatureMongo(mock_cache)
            result = use_case.handle({'feature_flag': 'ENV_HI'})

            self.assertEqual(result.http_code, 200)
            self.assertEqual(result.message, 'ok')
            mock_result_repo.delete.assert_called_with()
            mock_cache.delete.assert_called_with(
                'ENV_HI'
            )

    @patch('flask_feature_flag.use_cases.FeatureRepository.find_one')
    def test_ok_bad_request(self, mock_repo):
        self._app = Flask(__name__)
        self._app.config['FEATURE_FLAG_TYPE'] = 'MONGO'

        with self._app.app_context():
            use_case = DeleteFeatureMongo(Mock())
            result = use_case.handle({})

            self.assertEqual(result.http_code, 400)
            self.assertEqual(result.message, 'bad_request')
