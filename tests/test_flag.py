from unittest import TestCase
from unittest.mock import patch, Mock
import click
from flask_feature_flag.utils import (
    response_not_found,
    Response
    )
from flask_feature_flag.flag import Flag

flag = Flag(
    Mock(config={'ENV_HI': True}),
    Mock()
    )


class TestIsEnabled(TestCase):

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_enable(self, mock_use_case):
        mock_use_case.return_value = True
        @flag.is_enabled(response_not_found, 'ENV_HI')
        def hi(name):
            return {'data': name, 'message': 'ok'}

        result = hi('success')

        self.assertEqual(result, {'data': 'success', 'message': 'ok'})
        self.assertEqual(hi.__name__, 'hi')

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_disable(self, mock_use_case):
        mock_use_case.return_value = False
        @flag.is_enabled(response_not_found, 'ENV_HI')
        def hi(name):
            pass

        result = hi('success')

        self.assertEqual(result, ({'message': 'NOT_FOUND'}, 404))
        self.assertEqual(hi.__name__, 'hi')


class TestCommandEnabled(TestCase):

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_enable(self, mock_use_case):
        mock_use_case.return_value = True
        @flag.command_enabled('ENV_RUN_COMMAND')
        def run_command():
            click.echo('I am command')

        result = run_command()

        self.assertEqual(run_command.__name__, 'run_command')
        self.assertIsNone(result)


class TestRouteEnabled(TestCase):

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_enable(self, mock_use_case):
        mock_use_case.return_value = True
        @flag.route_enabled('ENV_RUN_ROUTE')
        def run_route():
            return dict(message='OK'), 200

        result = run_route()

        self.assertEqual(result, ({'message': 'OK'}, 200))
        self.assertEqual(run_route.__name__, 'run_route')

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_disable(self, mock_use_case):
        mock_use_case.return_value = False
        @flag.route_enabled('ENV_RUN_ROUTE')
        def run_route():
            pass

        result = run_route()
        self.assertEqual(result, ({'message': 'NOT_FOUND'}, 404))
        self.assertEqual(run_route.__name__, 'run_route')


class TestUseCaseEnabled(TestCase):

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_enable(self, mock_use_case):
        mock_use_case.return_value = True
        @flag.use_case_enabled('ENV_RUN_USE_CASE')
        def run_use_case():
            return Response(
                http_code=200,
                message='OK'
            )

        result = run_use_case()

        self.assertIsInstance(result, Response)
        self.assertEqual(result.http_code, 200)
        self.assertEqual(run_use_case.__name__, 'run_use_case')

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_disable(self, mock_use_case):
        mock_use_case.return_value = False

        @flag.use_case_enabled('ENV_RUN_USE_CASE')
        def run_use_case():
            pass

        result = run_use_case()

        self.assertIsInstance(result, Response)
        self.assertEqual(result.http_code, 404)
        self.assertEqual(run_use_case.__name__, 'run_use_case')
        mock_use_case.assert_called_with('ENV_RUN_USE_CASE')


class TestFunctions(TestCase):

    @patch('flask_feature_flag.use_cases.GetFeature.handle')
    def test_flag_on(self, mock_use_case):
        mock_use_case.return_value = True
        result = flag.on('ENV_HI')

        self.assertEqual(result, True)
