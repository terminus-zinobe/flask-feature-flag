from unittest import TestCase
from mongoengine import connect, disconnect
from flask_feature_flag.models import FeatureFlag
from flask_feature_flag.repository import FeatureRepository


class TestFeatureRepository(TestCase):

    @classmethod
    def setUpClass(self):
        disconnect()
        connect('mongoenginetest', host='mongomock://localhost')

        FeatureFlag(
            feature_flag='LOGING',
            feature_flag_value=True
        ).save()

        FeatureFlag(
            feature_flag='LOGOUT',
            feature_flag_value=True
        ).save()

    @classmethod
    def tearDownClass(self):
        disconnect()

    def test_find_one(self):
        result = FeatureRepository.find_one(feature_flag='LOGOUT')

        self.assertEqual(result.feature_flag, 'LOGOUT')
        self.assertEqual(result.feature_flag_value, True)
        self.assertIsInstance(result, FeatureFlag)

    def test_find_all(self):
        result = FeatureRepository.find_all()

        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], FeatureFlag)
